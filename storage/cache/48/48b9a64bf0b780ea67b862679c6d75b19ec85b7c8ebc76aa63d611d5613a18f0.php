<?php

/* extension/shipping/usps.twig */
class __TwigTemplate_1cbe7155334aa33dc14d85e0ccb201cbf166c0fc063e9d15c5ead030749097b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-shipping\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-shipping\" class=\"form-horizontal\">
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-user-id\">";
        // line 29
        echo (isset($context["entry_user_id"]) ? $context["entry_user_id"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_usps_user_id\" value=\"";
        // line 31
        echo (isset($context["shipping_usps_user_id"]) ? $context["shipping_usps_user_id"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_user_id"]) ? $context["entry_user_id"] : null);
        echo "\" id=\"input-user-id\" class=\"form-control\" />
              ";
        // line 32
        if ((isset($context["error_user_id"]) ? $context["error_user_id"] : null)) {
            // line 33
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_user_id"]) ? $context["error_user_id"] : null);
            echo "</div>
              ";
        }
        // line 35
        echo "            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-postcode\">";
        // line 38
        echo (isset($context["entry_postcode"]) ? $context["entry_postcode"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_usps_postcode\" value=\"";
        // line 40
        echo (isset($context["shipping_usps_postcode"]) ? $context["shipping_usps_postcode"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_postcode"]) ? $context["entry_postcode"] : null);
        echo "\" id=\"input-postcode\" class=\"form-control\" />
              ";
        // line 41
        if ((isset($context["error_postcode"]) ? $context["error_postcode"] : null)) {
            // line 42
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_postcode"]) ? $context["error_postcode"] : null);
            echo "</div>
              ";
        }
        // line 44
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 47
        echo (isset($context["entry_domestic"]) ? $context["entry_domestic"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 52
        if ((isset($context["shipping_usps_domestic_00"]) ? $context["shipping_usps_domestic_00"] : null)) {
            // line 53
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_00\" value=\"1\" checked=\"checked\" />
                    ";
            // line 54
            echo (isset($context["text_domestic_00"]) ? $context["text_domestic_00"] : null);
            echo "
                    ";
        } else {
            // line 56
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_00\" value=\"1\" />
                    ";
            // line 57
            echo (isset($context["text_domestic_00"]) ? $context["text_domestic_00"] : null);
            echo "
                    ";
        }
        // line 59
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 63
        if ((isset($context["shipping_usps_domestic_01"]) ? $context["shipping_usps_domestic_01"] : null)) {
            // line 64
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_01\" value=\"1\" checked=\"checked\" />
                    ";
            // line 65
            echo (isset($context["text_domestic_01"]) ? $context["text_domestic_01"] : null);
            echo "
                    ";
        } else {
            // line 67
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_01\" value=\"1\" />
                    ";
            // line 68
            echo (isset($context["text_domestic_01"]) ? $context["text_domestic_01"] : null);
            echo "
                    ";
        }
        // line 70
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 74
        if ((isset($context["shipping_usps_domestic_02"]) ? $context["shipping_usps_domestic_02"] : null)) {
            // line 75
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_02\" value=\"1\" checked=\"checked\" />
                    ";
            // line 76
            echo (isset($context["text_domestic_02"]) ? $context["text_domestic_02"] : null);
            echo "
                    ";
        } else {
            // line 78
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_02\" value=\"1\" />
                    ";
            // line 79
            echo (isset($context["text_domestic_02"]) ? $context["text_domestic_02"] : null);
            echo "
                    ";
        }
        // line 81
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 85
        if ((isset($context["shipping_usps_domestic_03"]) ? $context["shipping_usps_domestic_03"] : null)) {
            // line 86
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_03\" value=\"1\" checked=\"checked\" />
                    ";
            // line 87
            echo (isset($context["text_domestic_03"]) ? $context["text_domestic_03"] : null);
            echo "
                    ";
        } else {
            // line 89
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_03\" value=\"1\" />
                    ";
            // line 90
            echo (isset($context["text_domestic_03"]) ? $context["text_domestic_03"] : null);
            echo "
                    ";
        }
        // line 92
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 96
        if ((isset($context["shipping_usps_domestic_1"]) ? $context["shipping_usps_domestic_1"] : null)) {
            // line 97
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_1\" value=\"1\" checked=\"checked\" />
                    ";
            // line 98
            echo (isset($context["text_domestic_1"]) ? $context["text_domestic_1"] : null);
            echo "
                    ";
        } else {
            // line 100
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_1\" value=\"1\" />
                    ";
            // line 101
            echo (isset($context["text_domestic_1"]) ? $context["text_domestic_1"] : null);
            echo "
                    ";
        }
        // line 103
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 107
        if ((isset($context["shipping_usps_domestic_2"]) ? $context["shipping_usps_domestic_2"] : null)) {
            // line 108
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_2\" value=\"1\" checked=\"checked\" />
                    ";
            // line 109
            echo (isset($context["text_domestic_2"]) ? $context["text_domestic_2"] : null);
            echo "
                    ";
        } else {
            // line 111
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_2\" value=\"1\" />
                    ";
            // line 112
            echo (isset($context["text_domestic_2"]) ? $context["text_domestic_2"] : null);
            echo "
                    ";
        }
        // line 114
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 118
        if ((isset($context["shipping_usps_domestic_3"]) ? $context["shipping_usps_domestic_3"] : null)) {
            // line 119
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_3\" value=\"1\" checked=\"checked\" />
                    ";
            // line 120
            echo (isset($context["text_domestic_3"]) ? $context["text_domestic_3"] : null);
            echo "
                    ";
        } else {
            // line 122
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_3\" value=\"1\" />
                    ";
            // line 123
            echo (isset($context["text_domestic_3"]) ? $context["text_domestic_3"] : null);
            echo "
                    ";
        }
        // line 125
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 129
        if ((isset($context["shipping_usps_domestic_4"]) ? $context["shipping_usps_domestic_4"] : null)) {
            // line 130
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_4\" value=\"1\" checked=\"checked\" />
                    ";
            // line 131
            echo (isset($context["text_domestic_4"]) ? $context["text_domestic_4"] : null);
            echo "
                    ";
        } else {
            // line 133
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_4\" value=\"1\" />
                    ";
            // line 134
            echo (isset($context["text_domestic_4"]) ? $context["text_domestic_4"] : null);
            echo "
                    ";
        }
        // line 136
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 140
        if ((isset($context["shipping_usps_domestic_5"]) ? $context["shipping_usps_domestic_5"] : null)) {
            // line 141
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_5\" value=\"1\" checked=\"checked\" />
                    ";
            // line 142
            echo (isset($context["text_domestic_5"]) ? $context["text_domestic_5"] : null);
            echo "
                    ";
        } else {
            // line 144
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_5\" value=\"1\" />
                    ";
            // line 145
            echo (isset($context["text_domestic_5"]) ? $context["text_domestic_5"] : null);
            echo "
                    ";
        }
        // line 147
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 151
        if ((isset($context["shipping_usps_domestic_6"]) ? $context["shipping_usps_domestic_6"] : null)) {
            // line 152
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_6\" value=\"1\" checked=\"checked\" />
                    ";
            // line 153
            echo (isset($context["text_domestic_6"]) ? $context["text_domestic_6"] : null);
            echo "
                    ";
        } else {
            // line 155
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_6\" value=\"1\" />
                    ";
            // line 156
            echo (isset($context["text_domestic_6"]) ? $context["text_domestic_6"] : null);
            echo "
                    ";
        }
        // line 158
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 162
        if ((isset($context["shipping_usps_domestic_7"]) ? $context["shipping_usps_domestic_7"] : null)) {
            // line 163
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_7\" value=\"1\" checked=\"checked\" />
                    ";
            // line 164
            echo (isset($context["text_domestic_7"]) ? $context["text_domestic_7"] : null);
            echo "
                    ";
        } else {
            // line 166
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_7\" value=\"1\" />
                    ";
            // line 167
            echo (isset($context["text_domestic_7"]) ? $context["text_domestic_7"] : null);
            echo "
                    ";
        }
        // line 169
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 173
        if ((isset($context["shipping_usps_domestic_12"]) ? $context["shipping_usps_domestic_12"] : null)) {
            // line 174
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_12\" value=\"1\" checked=\"checked\" />
                    ";
            // line 175
            echo (isset($context["text_domestic_12"]) ? $context["text_domestic_12"] : null);
            echo "
                    ";
        } else {
            // line 177
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_12\" value=\"1\" />
                    ";
            // line 178
            echo (isset($context["text_domestic_12"]) ? $context["text_domestic_12"] : null);
            echo "
                    ";
        }
        // line 180
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 184
        if ((isset($context["shipping_usps_domestic_13"]) ? $context["shipping_usps_domestic_13"] : null)) {
            // line 185
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_13\" value=\"1\" checked=\"checked\" />
                    ";
            // line 186
            echo (isset($context["text_domestic_13"]) ? $context["text_domestic_13"] : null);
            echo "
                    ";
        } else {
            // line 188
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_13\" value=\"1\" />
                    ";
            // line 189
            echo (isset($context["text_domestic_13"]) ? $context["text_domestic_13"] : null);
            echo "
                    ";
        }
        // line 191
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 195
        if ((isset($context["shipping_usps_domestic_16"]) ? $context["shipping_usps_domestic_16"] : null)) {
            // line 196
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_16\" value=\"1\" checked=\"checked\" />
                    ";
            // line 197
            echo (isset($context["text_domestic_16"]) ? $context["text_domestic_16"] : null);
            echo "
                    ";
        } else {
            // line 199
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_16\" value=\"1\" />
                    ";
            // line 200
            echo (isset($context["text_domestic_16"]) ? $context["text_domestic_16"] : null);
            echo "
                    ";
        }
        // line 202
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 206
        if ((isset($context["shipping_usps_domestic_17"]) ? $context["shipping_usps_domestic_17"] : null)) {
            // line 207
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_17\" value=\"1\" checked=\"checked\" />
                    ";
            // line 208
            echo (isset($context["text_domestic_17"]) ? $context["text_domestic_17"] : null);
            echo "
                    ";
        } else {
            // line 210
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_17\" value=\"1\" />
                    ";
            // line 211
            echo (isset($context["text_domestic_17"]) ? $context["text_domestic_17"] : null);
            echo "
                    ";
        }
        // line 213
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 217
        if ((isset($context["shipping_usps_domestic_18"]) ? $context["shipping_usps_domestic_18"] : null)) {
            // line 218
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_18\" value=\"1\" checked=\"checked\" />
                    ";
            // line 219
            echo (isset($context["text_domestic_18"]) ? $context["text_domestic_18"] : null);
            echo "
                    ";
        } else {
            // line 221
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_18\" value=\"1\" />
                    ";
            // line 222
            echo (isset($context["text_domestic_18"]) ? $context["text_domestic_18"] : null);
            echo "
                    ";
        }
        // line 224
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 228
        if ((isset($context["shipping_usps_domestic_19"]) ? $context["shipping_usps_domestic_19"] : null)) {
            // line 229
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_19\" value=\"1\" checked=\"checked\" />
                    ";
            // line 230
            echo (isset($context["text_domestic_19"]) ? $context["text_domestic_19"] : null);
            echo "
                    ";
        } else {
            // line 232
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_19\" value=\"1\" />
                    ";
            // line 233
            echo (isset($context["text_domestic_19"]) ? $context["text_domestic_19"] : null);
            echo "
                    ";
        }
        // line 235
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 239
        if ((isset($context["shipping_usps_domestic_22"]) ? $context["shipping_usps_domestic_22"] : null)) {
            // line 240
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_22\" value=\"1\" checked=\"checked\" />
                    ";
            // line 241
            echo (isset($context["text_domestic_22"]) ? $context["text_domestic_22"] : null);
            echo "
                    ";
        } else {
            // line 243
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_22\" value=\"1\" />
                    ";
            // line 244
            echo (isset($context["text_domestic_22"]) ? $context["text_domestic_22"] : null);
            echo "
                   ";
        }
        // line 246
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 250
        if ((isset($context["shipping_usps_domestic_23"]) ? $context["shipping_usps_domestic_23"] : null)) {
            // line 251
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_23\" value=\"1\" checked=\"checked\" />
                    ";
            // line 252
            echo (isset($context["text_domestic_23"]) ? $context["text_domestic_23"] : null);
            echo "
                    ";
        } else {
            // line 254
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_23\" value=\"1\" />
                    ";
            // line 255
            echo (isset($context["text_domestic_23"]) ? $context["text_domestic_23"] : null);
            echo "
                    ";
        }
        // line 257
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 261
        if ((isset($context["shipping_usps_domestic_25"]) ? $context["shipping_usps_domestic_25"] : null)) {
            // line 262
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_25\" value=\"1\" checked=\"checked\" />
                    ";
            // line 263
            echo (isset($context["text_domestic_25"]) ? $context["text_domestic_25"] : null);
            echo "
                    ";
        } else {
            // line 265
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_25\" value=\"1\" />
                    ";
            // line 266
            echo (isset($context["text_domestic_25"]) ? $context["text_domestic_25"] : null);
            echo "
                    ";
        }
        // line 268
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 272
        if ((isset($context["shipping_usps_domestic_27"]) ? $context["shipping_usps_domestic_27"] : null)) {
            // line 273
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_27\" value=\"1\" checked=\"checked\" />
                    ";
            // line 274
            echo (isset($context["text_domestic_27"]) ? $context["text_domestic_27"] : null);
            echo "
                    ";
        } else {
            // line 276
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_27\" value=\"1\" />
                    ";
            // line 277
            echo (isset($context["text_domestic_27"]) ? $context["text_domestic_27"] : null);
            echo "
                    ";
        }
        // line 279
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 283
        if ((isset($context["shipping_usps_domestic_28"]) ? $context["shipping_usps_domestic_28"] : null)) {
            // line 284
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_28\" value=\"1\" checked=\"checked\" />
                    ";
            // line 285
            echo (isset($context["text_domestic_28"]) ? $context["text_domestic_28"] : null);
            echo "
                    ";
        } else {
            // line 287
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_domestic_28\" value=\"1\" />
                    ";
            // line 288
            echo (isset($context["text_domestic_28"]) ? $context["text_domestic_28"] : null);
            echo "
                    ";
        }
        // line 290
        echo "                  </label>
                </div>
              </div>
              <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', true);\" class=\"btn btn-link\">";
        // line 293
        echo (isset($context["text_select_all"]) ? $context["text_select_all"] : null);
        echo "</button> / <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', false);\" class=\"btn btn-link\">";
        echo (isset($context["text_unselect_all"]) ? $context["text_unselect_all"] : null);
        echo "</button></div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 296
        echo (isset($context["entry_international"]) ? $context["entry_international"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 301
        if ((isset($context["shipping_usps_international_1"]) ? $context["shipping_usps_international_1"] : null)) {
            // line 302
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_1\" value=\"1\" checked=\"checked\" />
                    ";
            // line 303
            echo (isset($context["text_international_1"]) ? $context["text_international_1"] : null);
            echo "
                    ";
        } else {
            // line 305
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_1\" value=\"1\" />
                    ";
            // line 306
            echo (isset($context["text_international_1"]) ? $context["text_international_1"] : null);
            echo "
                    ";
        }
        // line 308
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 312
        if ((isset($context["shipping_usps_international_2"]) ? $context["shipping_usps_international_2"] : null)) {
            // line 313
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_2\" value=\"1\" checked=\"checked\" />
                    ";
            // line 314
            echo (isset($context["text_international_2"]) ? $context["text_international_2"] : null);
            echo "
                    ";
        } else {
            // line 316
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_2\" value=\"1\" />
                    ";
            // line 317
            echo (isset($context["text_international_2"]) ? $context["text_international_2"] : null);
            echo "
                    ";
        }
        // line 319
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 323
        if ((isset($context["shipping_usps_international_4"]) ? $context["shipping_usps_international_4"] : null)) {
            // line 324
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_4\" value=\"1\" checked=\"checked\" />
                    ";
            // line 325
            echo (isset($context["text_international_4"]) ? $context["text_international_4"] : null);
            echo "
                    ";
        } else {
            // line 327
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_4\" value=\"1\" />
                    ";
            // line 328
            echo (isset($context["text_international_4"]) ? $context["text_international_4"] : null);
            echo "
                    ";
        }
        // line 330
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 334
        if ((isset($context["shipping_usps_international_5"]) ? $context["shipping_usps_international_5"] : null)) {
            // line 335
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_5\" value=\"1\" checked=\"checked\" />
                    ";
            // line 336
            echo (isset($context["text_international_5"]) ? $context["text_international_5"] : null);
            echo "
                    ";
        } else {
            // line 338
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_5\" value=\"1\" />
                    ";
            // line 339
            echo (isset($context["text_international_5"]) ? $context["text_international_5"] : null);
            echo "
                    ";
        }
        // line 341
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 345
        if ((isset($context["shipping_usps_international_6"]) ? $context["shipping_usps_international_6"] : null)) {
            // line 346
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_6\" value=\"1\" checked=\"checked\" />
                    ";
            // line 347
            echo (isset($context["text_international_6"]) ? $context["text_international_6"] : null);
            echo "
                    ";
        } else {
            // line 349
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_6\" value=\"1\" />
                    ";
            // line 350
            echo (isset($context["text_international_6"]) ? $context["text_international_6"] : null);
            echo "
                    ";
        }
        // line 352
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 356
        if ((isset($context["shipping_usps_international_7"]) ? $context["shipping_usps_international_7"] : null)) {
            // line 357
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_7\" value=\"1\" checked=\"checked\" />
                    ";
            // line 358
            echo (isset($context["text_international_7"]) ? $context["text_international_7"] : null);
            echo "
                    ";
        } else {
            // line 360
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_7\" value=\"1\" />
                    ";
            // line 361
            echo (isset($context["text_international_7"]) ? $context["text_international_7"] : null);
            echo "
                    ";
        }
        // line 363
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 367
        if ((isset($context["shipping_usps_international_8"]) ? $context["shipping_usps_international_8"] : null)) {
            // line 368
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_8\" value=\"1\" checked=\"checked\" />
                    ";
            // line 369
            echo (isset($context["text_international_8"]) ? $context["text_international_8"] : null);
            echo "
                    ";
        } else {
            // line 371
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_8\" value=\"1\" />
                    ";
            // line 372
            echo (isset($context["text_international_8"]) ? $context["text_international_8"] : null);
            echo "
                    ";
        }
        // line 374
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 378
        if ((isset($context["shipping_usps_international_9"]) ? $context["shipping_usps_international_9"] : null)) {
            // line 379
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_9\" value=\"1\" checked=\"checked\" />
                    ";
            // line 380
            echo (isset($context["text_international_9"]) ? $context["text_international_9"] : null);
            echo "
                    ";
        } else {
            // line 382
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_9\" value=\"1\" />
                    ";
            // line 383
            echo (isset($context["text_international_9"]) ? $context["text_international_9"] : null);
            echo "
                    ";
        }
        // line 385
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 389
        if ((isset($context["shipping_usps_international_10"]) ? $context["shipping_usps_international_10"] : null)) {
            // line 390
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_10\" value=\"1\" checked=\"checked\" />
                    ";
            // line 391
            echo (isset($context["text_international_10"]) ? $context["text_international_10"] : null);
            echo "
                    ";
        } else {
            // line 393
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_10\" value=\"1\" />
                    ";
            // line 394
            echo (isset($context["text_international_10"]) ? $context["text_international_10"] : null);
            echo "
                    ";
        }
        // line 396
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 400
        if ((isset($context["shipping_usps_international_11"]) ? $context["shipping_usps_international_11"] : null)) {
            // line 401
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_11\" value=\"1\" checked=\"checked\" />
                    ";
            // line 402
            echo (isset($context["text_international_11"]) ? $context["text_international_11"] : null);
            echo "
                    ";
        } else {
            // line 404
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_11\" value=\"1\" />
                    ";
            // line 405
            echo (isset($context["text_international_11"]) ? $context["text_international_11"] : null);
            echo "
                    ";
        }
        // line 407
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 411
        if ((isset($context["shipping_usps_international_12"]) ? $context["shipping_usps_international_12"] : null)) {
            // line 412
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_12\" value=\"1\" checked=\"checked\" />
                    ";
            // line 413
            echo (isset($context["text_international_12"]) ? $context["text_international_12"] : null);
            echo "
                    ";
        } else {
            // line 415
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_12\" value=\"1\" />
                    ";
            // line 416
            echo (isset($context["text_international_12"]) ? $context["text_international_12"] : null);
            echo "
                    ";
        }
        // line 418
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 422
        if ((isset($context["shipping_usps_international_13"]) ? $context["shipping_usps_international_13"] : null)) {
            // line 423
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_13\" value=\"1\" checked=\"checked\" />
                    ";
            // line 424
            echo (isset($context["text_international_13"]) ? $context["text_international_13"] : null);
            echo "
                    ";
        } else {
            // line 426
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_13\" value=\"1\" />
                    ";
            // line 427
            echo (isset($context["text_international_13"]) ? $context["text_international_13"] : null);
            echo "
                    ";
        }
        // line 429
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 433
        if ((isset($context["shipping_usps_international_14"]) ? $context["shipping_usps_international_14"] : null)) {
            // line 434
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_14\" value=\"1\" checked=\"checked\" />
                    ";
            // line 435
            echo (isset($context["text_international_14"]) ? $context["text_international_14"] : null);
            echo "
                    ";
        } else {
            // line 437
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_14\" value=\"1\" />
                    ";
            // line 438
            echo (isset($context["text_international_14"]) ? $context["text_international_14"] : null);
            echo "
                    ";
        }
        // line 440
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 444
        if ((isset($context["shipping_usps_international_15"]) ? $context["shipping_usps_international_15"] : null)) {
            // line 445
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_15\" value=\"1\" checked=\"checked\" />
                    ";
            // line 446
            echo (isset($context["text_international_15"]) ? $context["text_international_15"] : null);
            echo "
                    ";
        } else {
            // line 448
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_15\" value=\"1\" />
                    ";
            // line 449
            echo (isset($context["text_international_15"]) ? $context["text_international_15"] : null);
            echo "
                    ";
        }
        // line 451
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 455
        if ((isset($context["shipping_usps_international_16"]) ? $context["shipping_usps_international_16"] : null)) {
            // line 456
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_16\" value=\"1\" checked=\"checked\" />
                    ";
            // line 457
            echo (isset($context["text_international_16"]) ? $context["text_international_16"] : null);
            echo "
                    ";
        } else {
            // line 459
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_16\" value=\"1\" />
                    ";
            // line 460
            echo (isset($context["text_international_16"]) ? $context["text_international_16"] : null);
            echo "
                    ";
        }
        // line 462
        echo "                  </label>
                </div>
                <div class=\"checkbox\">
                  <label>
                    ";
        // line 466
        if ((isset($context["shipping_usps_international_21"]) ? $context["shipping_usps_international_21"] : null)) {
            // line 467
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_21\" value=\"1\" checked=\"checked\" />
                    ";
            // line 468
            echo (isset($context["text_international_21"]) ? $context["text_international_21"] : null);
            echo "
                    ";
        } else {
            // line 470
            echo "                    <input type=\"checkbox\" name=\"shipping_usps_international_21\" value=\"1\" />
                    ";
            // line 471
            echo (isset($context["text_international_21"]) ? $context["text_international_21"] : null);
            echo "
                    ";
        }
        // line 473
        echo "                  </label>
                </div>
              </div>
              <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', true);\" class=\"btn btn-link\">";
        // line 476
        echo (isset($context["text_select_all"]) ? $context["text_select_all"] : null);
        echo "</button> / <button type=\"button\" onclick=\"\$(this).parent().find(':checkbox').prop('checked', false);\" class=\"btn btn-link\">";
        echo (isset($context["text_unselect_all"]) ? $context["text_unselect_all"] : null);
        echo "</button></div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-size\">";
        // line 479
        echo (isset($context["entry_size"]) ? $context["entry_size"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_size\" id=\"input-size\" class=\"form-control\">
                ";
        // line 482
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sizes"]) ? $context["sizes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["size"]) {
            // line 483
            echo "                ";
            if (($this->getAttribute($context["size"], "value", array()) == (isset($context["shipping_usps_size"]) ? $context["shipping_usps_size"] : null))) {
                // line 484
                echo "                <option value=\"";
                echo $this->getAttribute($context["size"], "value", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["size"], "text", array());
                echo "</option>
                ";
            } else {
                // line 486
                echo "                <option value=\"";
                echo $this->getAttribute($context["size"], "value", array());
                echo "\">";
                echo $this->getAttribute($context["size"], "text", array());
                echo "</option>
               ";
            }
            // line 488
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['size'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 489
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-container\">";
        // line 493
        echo (isset($context["entry_container"]) ? $context["entry_container"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_container\" id=\"input-container\" class=\"form-control\">
                ";
        // line 496
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["containers"]) ? $context["containers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["container"]) {
            // line 497
            echo "                ";
            if (($this->getAttribute($context["container"], "value", array()) == (isset($context["shipping_usps_container"]) ? $context["shipping_usps_container"] : null))) {
                // line 498
                echo "                <option value=\"";
                echo $this->getAttribute($context["container"], "value", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["container"], "text", array());
                echo "</option>
                ";
            } else {
                // line 500
                echo "                <option value=\"";
                echo $this->getAttribute($context["container"], "value", array());
                echo "\">";
                echo $this->getAttribute($context["container"], "text", array());
                echo "</option>
                ";
            }
            // line 502
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['container'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 503
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-machinable\">";
        // line 507
        echo (isset($context["entry_machinable"]) ? $context["entry_machinable"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_machinable\" id=\"input-machinable\" class=\"form-control\">
                ";
        // line 510
        if ((isset($context["shipping_usps_machinable"]) ? $context["shipping_usps_machinable"] : null)) {
            // line 511
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 512
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                ";
        } else {
            // line 514
            echo "                <option value=\"1\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 515
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                ";
        }
        // line 517
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-length\"><span data-toggle=\"tooltip\" title=\"";
        // line 521
        echo (isset($context["help_dimension"]) ? $context["help_dimension"] : null);
        echo "\">";
        echo (isset($context["entry_dimension"]) ? $context["entry_dimension"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <div class=\"row\">
                <div class=\"col-sm-4\">
                  <input type=\"text\" name=\"shipping_usps_length\" value=\"";
        // line 525
        echo (isset($context["shipping_usps_length"]) ? $context["shipping_usps_length"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_length"]) ? $context["entry_length"] : null);
        echo "\" id=\"input-length\" class=\"form-control\" />
                </div>
                <div class=\"col-sm-4\">
                  <input type=\"text\" name=\"shipping_usps_width\" value=\"";
        // line 528
        echo (isset($context["shipping_usps_width"]) ? $context["shipping_usps_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-width\" class=\"form-control\" />
                </div>
                <div class=\"col-sm-4\">
                  <input type=\"text\" name=\"shipping_usps_height\" value=\"";
        // line 531
        echo (isset($context["shipping_usps_height"]) ? $context["shipping_usps_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" id=\"input-height\" class=\"form-control\" />
                </div>
              </div>
              ";
        // line 534
        if ((isset($context["error_dimension"]) ? $context["error_dimension"] : null)) {
            // line 535
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_dimension"]) ? $context["error_dimension"] : null);
            echo "</div>
              ";
        }
        // line 537
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 540
        echo (isset($context["help_display_time"]) ? $context["help_display_time"] : null);
        echo "\">";
        echo (isset($context["entry_display_time"]) ? $context["entry_display_time"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <label class=\"radio-inline\">
                ";
        // line 543
        if ((isset($context["shipping_usps_display_time"]) ? $context["shipping_usps_display_time"] : null)) {
            // line 544
            echo "                <input type=\"radio\" name=\"shipping_usps_display_time\" value=\"1\" checked=\"checked\" />
                ";
            // line 545
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        } else {
            // line 547
            echo "                <input type=\"radio\" name=\"shipping_usps_display_time\" value=\"1\" />
                ";
            // line 548
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        }
        // line 550
        echo "              </label>
              <label class=\"radio-inline\">
                ";
        // line 552
        if ( !(isset($context["shipping_usps_display_time"]) ? $context["shipping_usps_display_time"] : null)) {
            // line 553
            echo "                <input type=\"radio\" name=\"shipping_usps_display_time\" value=\"0\" checked=\"checked\" />
                ";
            // line 554
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        } else {
            // line 556
            echo "                <input type=\"radio\" name=\"shipping_usps_display_time\" value=\"0\" />
                ";
            // line 557
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        }
        // line 559
        echo "              </label>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 563
        echo (isset($context["help_display_weight"]) ? $context["help_display_weight"] : null);
        echo "\">";
        echo (isset($context["entry_display_weight"]) ? $context["entry_display_weight"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <label class=\"radio-inline\">
                ";
        // line 566
        if ((isset($context["shipping_usps_display_weight"]) ? $context["shipping_usps_display_weight"] : null)) {
            // line 567
            echo "                <input type=\"radio\" name=\"shipping_usps_display_weight\" value=\"1\" checked=\"checked\" />
                ";
            // line 568
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        } else {
            // line 570
            echo "                <input type=\"radio\" name=\"shipping_usps_display_weight\" value=\"1\" />
                ";
            // line 571
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        }
        // line 573
        echo "              </label>
              <label class=\"radio-inline\">
                ";
        // line 575
        if ( !(isset($context["shipping_usps_display_weight"]) ? $context["shipping_usps_display_weight"] : null)) {
            // line 576
            echo "                <input type=\"radio\" name=\"shipping_usps_display_weight\" value=\"0\" checked=\"checked\" />
                ";
            // line 577
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        } else {
            // line 579
            echo "                <input type=\"radio\" name=\"shipping_usps_display_weight\" value=\"0\" />
                ";
            // line 580
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        }
        // line 582
        echo "              </label>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-weight-class\"><span data-toggle=\"tooltip\" title=\"";
        // line 586
        echo (isset($context["help_weight_class"]) ? $context["help_weight_class"] : null);
        echo "\">";
        echo (isset($context["entry_weight_class"]) ? $context["entry_weight_class"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_weight_class_id\" id=\"input-weight-class\" class=\"form-control\">
                ";
        // line 589
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["weight_classes"]) ? $context["weight_classes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["weight_class"]) {
            // line 590
            echo "                ";
            if (($this->getAttribute($context["weight_class"], "weight_class_id", array()) == (isset($context["shipping_usps_weight_class_id"]) ? $context["shipping_usps_weight_class_id"] : null))) {
                // line 591
                echo "                <option value=\"";
                echo $this->getAttribute($context["weight_class"], "weight_class_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["weight_class"], "title", array());
                echo "</option>
                ";
            } else {
                // line 593
                echo "                <option value=\"";
                echo $this->getAttribute($context["weight_class"], "weight_class_id", array());
                echo "\">";
                echo $this->getAttribute($context["weight_class"], "title", array());
                echo "</option>
                ";
            }
            // line 595
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['weight_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 596
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-tax-class\">";
        // line 600
        echo (isset($context["entry_tax"]) ? $context["entry_tax"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_tax_class_id\" id=\"input-tax-class\" class=\"form-control\">
                <option value=\"0\">";
        // line 603
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "</option>
                ";
        // line 604
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tax_classes"]) ? $context["tax_classes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tax_class"]) {
            // line 605
            echo "                ";
            if (($this->getAttribute($context["tax_class"], "tax_class_id", array()) == (isset($context["shipping_usps_tax_class_id"]) ? $context["shipping_usps_tax_class_id"] : null))) {
                // line 606
                echo "                <option value=\"";
                echo $this->getAttribute($context["tax_class"], "tax_class_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["tax_class"], "title", array());
                echo "</option>
                ";
            } else {
                // line 608
                echo "                <option value=\"";
                echo $this->getAttribute($context["tax_class"], "tax_class_id", array());
                echo "\">";
                echo $this->getAttribute($context["tax_class"], "title", array());
                echo "</option>
                ";
            }
            // line 610
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tax_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 611
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">";
        // line 615
        echo (isset($context["entry_geo_zone"]) ? $context["entry_geo_zone"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                <option value=\"0\">";
        // line 618
        echo (isset($context["text_all_zones"]) ? $context["text_all_zones"] : null);
        echo "</option>
                ";
        // line 619
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["geo_zones"]) ? $context["geo_zones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["geo_zone"]) {
            // line 620
            echo "                ";
            if (($this->getAttribute($context["geo_zone"], "geo_zone_id", array()) == (isset($context["shipping_usps_geo_zone_id"]) ? $context["shipping_usps_geo_zone_id"] : null))) {
                // line 621
                echo "                <option value=\"";
                echo $this->getAttribute($context["geo_zone"], "geo_zone_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["geo_zone"], "name", array());
                echo "</option>
                ";
            } else {
                // line 623
                echo "                <option value=\"";
                echo $this->getAttribute($context["geo_zone"], "geo_zone_id", array());
                echo "\">";
                echo $this->getAttribute($context["geo_zone"], "name", array());
                echo "</option>
                ";
            }
            // line 625
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geo_zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 626
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 630
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 633
        if ((isset($context["shipping_usps_status"]) ? $context["shipping_usps_status"] : null)) {
            // line 634
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 635
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        } else {
            // line 637
            echo "                <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 638
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        }
        // line 640
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 644
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_usps_sort_order\" value=\"";
        // line 646
        echo (isset($context["shipping_usps_sort_order"]) ? $context["shipping_usps_sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input-sort-order\" class=\"form-control\" />
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-debug\"><span data-toggle=\"tooltip\" title=\"";
        // line 650
        echo (isset($context["help_debug"]) ? $context["help_debug"] : null);
        echo "\">";
        echo (isset($context["entry_debug"]) ? $context["entry_debug"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_usps_debug\" id=\"input-debug\" class=\"form-control\">
                ";
        // line 653
        if ((isset($context["shipping_usps_debug"]) ? $context["shipping_usps_debug"] : null)) {
            // line 654
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 655
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        } else {
            // line 657
            echo "                <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 658
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        }
        // line 660
        echo "              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 668
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/shipping/usps.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1570 => 668,  1560 => 660,  1555 => 658,  1550 => 657,  1545 => 655,  1540 => 654,  1538 => 653,  1530 => 650,  1521 => 646,  1516 => 644,  1510 => 640,  1505 => 638,  1500 => 637,  1495 => 635,  1490 => 634,  1488 => 633,  1482 => 630,  1476 => 626,  1470 => 625,  1462 => 623,  1454 => 621,  1451 => 620,  1447 => 619,  1443 => 618,  1437 => 615,  1431 => 611,  1425 => 610,  1417 => 608,  1409 => 606,  1406 => 605,  1402 => 604,  1398 => 603,  1392 => 600,  1386 => 596,  1380 => 595,  1372 => 593,  1364 => 591,  1361 => 590,  1357 => 589,  1349 => 586,  1343 => 582,  1338 => 580,  1335 => 579,  1330 => 577,  1327 => 576,  1325 => 575,  1321 => 573,  1316 => 571,  1313 => 570,  1308 => 568,  1305 => 567,  1303 => 566,  1295 => 563,  1289 => 559,  1284 => 557,  1281 => 556,  1276 => 554,  1273 => 553,  1271 => 552,  1267 => 550,  1262 => 548,  1259 => 547,  1254 => 545,  1251 => 544,  1249 => 543,  1241 => 540,  1236 => 537,  1230 => 535,  1228 => 534,  1220 => 531,  1212 => 528,  1204 => 525,  1195 => 521,  1189 => 517,  1184 => 515,  1179 => 514,  1174 => 512,  1169 => 511,  1167 => 510,  1161 => 507,  1155 => 503,  1149 => 502,  1141 => 500,  1133 => 498,  1130 => 497,  1126 => 496,  1120 => 493,  1114 => 489,  1108 => 488,  1100 => 486,  1092 => 484,  1089 => 483,  1085 => 482,  1079 => 479,  1071 => 476,  1066 => 473,  1061 => 471,  1058 => 470,  1053 => 468,  1050 => 467,  1048 => 466,  1042 => 462,  1037 => 460,  1034 => 459,  1029 => 457,  1026 => 456,  1024 => 455,  1018 => 451,  1013 => 449,  1010 => 448,  1005 => 446,  1002 => 445,  1000 => 444,  994 => 440,  989 => 438,  986 => 437,  981 => 435,  978 => 434,  976 => 433,  970 => 429,  965 => 427,  962 => 426,  957 => 424,  954 => 423,  952 => 422,  946 => 418,  941 => 416,  938 => 415,  933 => 413,  930 => 412,  928 => 411,  922 => 407,  917 => 405,  914 => 404,  909 => 402,  906 => 401,  904 => 400,  898 => 396,  893 => 394,  890 => 393,  885 => 391,  882 => 390,  880 => 389,  874 => 385,  869 => 383,  866 => 382,  861 => 380,  858 => 379,  856 => 378,  850 => 374,  845 => 372,  842 => 371,  837 => 369,  834 => 368,  832 => 367,  826 => 363,  821 => 361,  818 => 360,  813 => 358,  810 => 357,  808 => 356,  802 => 352,  797 => 350,  794 => 349,  789 => 347,  786 => 346,  784 => 345,  778 => 341,  773 => 339,  770 => 338,  765 => 336,  762 => 335,  760 => 334,  754 => 330,  749 => 328,  746 => 327,  741 => 325,  738 => 324,  736 => 323,  730 => 319,  725 => 317,  722 => 316,  717 => 314,  714 => 313,  712 => 312,  706 => 308,  701 => 306,  698 => 305,  693 => 303,  690 => 302,  688 => 301,  680 => 296,  672 => 293,  667 => 290,  662 => 288,  659 => 287,  654 => 285,  651 => 284,  649 => 283,  643 => 279,  638 => 277,  635 => 276,  630 => 274,  627 => 273,  625 => 272,  619 => 268,  614 => 266,  611 => 265,  606 => 263,  603 => 262,  601 => 261,  595 => 257,  590 => 255,  587 => 254,  582 => 252,  579 => 251,  577 => 250,  571 => 246,  566 => 244,  563 => 243,  558 => 241,  555 => 240,  553 => 239,  547 => 235,  542 => 233,  539 => 232,  534 => 230,  531 => 229,  529 => 228,  523 => 224,  518 => 222,  515 => 221,  510 => 219,  507 => 218,  505 => 217,  499 => 213,  494 => 211,  491 => 210,  486 => 208,  483 => 207,  481 => 206,  475 => 202,  470 => 200,  467 => 199,  462 => 197,  459 => 196,  457 => 195,  451 => 191,  446 => 189,  443 => 188,  438 => 186,  435 => 185,  433 => 184,  427 => 180,  422 => 178,  419 => 177,  414 => 175,  411 => 174,  409 => 173,  403 => 169,  398 => 167,  395 => 166,  390 => 164,  387 => 163,  385 => 162,  379 => 158,  374 => 156,  371 => 155,  366 => 153,  363 => 152,  361 => 151,  355 => 147,  350 => 145,  347 => 144,  342 => 142,  339 => 141,  337 => 140,  331 => 136,  326 => 134,  323 => 133,  318 => 131,  315 => 130,  313 => 129,  307 => 125,  302 => 123,  299 => 122,  294 => 120,  291 => 119,  289 => 118,  283 => 114,  278 => 112,  275 => 111,  270 => 109,  267 => 108,  265 => 107,  259 => 103,  254 => 101,  251 => 100,  246 => 98,  243 => 97,  241 => 96,  235 => 92,  230 => 90,  227 => 89,  222 => 87,  219 => 86,  217 => 85,  211 => 81,  206 => 79,  203 => 78,  198 => 76,  195 => 75,  193 => 74,  187 => 70,  182 => 68,  179 => 67,  174 => 65,  171 => 64,  169 => 63,  163 => 59,  158 => 57,  155 => 56,  150 => 54,  147 => 53,  145 => 52,  137 => 47,  132 => 44,  126 => 42,  124 => 41,  118 => 40,  113 => 38,  108 => 35,  102 => 33,  100 => 32,  94 => 31,  89 => 29,  84 => 27,  78 => 24,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-shipping" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-shipping" class="form-horizontal">*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-user-id">{{ entry_user_id }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="shipping_usps_user_id" value="{{ shipping_usps_user_id }}" placeholder="{{ entry_user_id }}" id="input-user-id" class="form-control" />*/
/*               {% if error_user_id %}*/
/*               <div class="text-danger">{{ error_user_id }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-postcode">{{ entry_postcode }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="shipping_usps_postcode" value="{{ shipping_usps_postcode }}" placeholder="{{ entry_postcode }}" id="input-postcode" class="form-control" />*/
/*               {% if error_postcode %}*/
/*               <div class="text-danger">{{ error_postcode }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label">{{ entry_domestic }}</label>*/
/*             <div class="col-sm-10">*/
/*               <div class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_00 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_00" value="1" checked="checked" />*/
/*                     {{ text_domestic_00 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_00" value="1" />*/
/*                     {{ text_domestic_00 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_01 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_01" value="1" checked="checked" />*/
/*                     {{ text_domestic_01 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_01" value="1" />*/
/*                     {{ text_domestic_01 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_02 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_02" value="1" checked="checked" />*/
/*                     {{ text_domestic_02 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_02" value="1" />*/
/*                     {{ text_domestic_02 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_03 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_03" value="1" checked="checked" />*/
/*                     {{ text_domestic_03 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_03" value="1" />*/
/*                     {{ text_domestic_03 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_1 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_1" value="1" checked="checked" />*/
/*                     {{ text_domestic_1 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_1" value="1" />*/
/*                     {{ text_domestic_1 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_2 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_2" value="1" checked="checked" />*/
/*                     {{ text_domestic_2 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_2" value="1" />*/
/*                     {{ text_domestic_2 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_3 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_3" value="1" checked="checked" />*/
/*                     {{ text_domestic_3 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_3" value="1" />*/
/*                     {{ text_domestic_3 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_4 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_4" value="1" checked="checked" />*/
/*                     {{ text_domestic_4 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_4" value="1" />*/
/*                     {{ text_domestic_4 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_5 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_5" value="1" checked="checked" />*/
/*                     {{ text_domestic_5 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_5" value="1" />*/
/*                     {{ text_domestic_5 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_6 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_6" value="1" checked="checked" />*/
/*                     {{ text_domestic_6 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_6" value="1" />*/
/*                     {{ text_domestic_6 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_7 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_7" value="1" checked="checked" />*/
/*                     {{ text_domestic_7 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_7" value="1" />*/
/*                     {{ text_domestic_7 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_12 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_12" value="1" checked="checked" />*/
/*                     {{ text_domestic_12 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_12" value="1" />*/
/*                     {{ text_domestic_12 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_13 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_13" value="1" checked="checked" />*/
/*                     {{ text_domestic_13 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_13" value="1" />*/
/*                     {{ text_domestic_13 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_16 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_16" value="1" checked="checked" />*/
/*                     {{ text_domestic_16 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_16" value="1" />*/
/*                     {{ text_domestic_16 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_17 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_17" value="1" checked="checked" />*/
/*                     {{ text_domestic_17 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_17" value="1" />*/
/*                     {{ text_domestic_17 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_18 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_18" value="1" checked="checked" />*/
/*                     {{ text_domestic_18 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_18" value="1" />*/
/*                     {{ text_domestic_18 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_19 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_19" value="1" checked="checked" />*/
/*                     {{ text_domestic_19 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_19" value="1" />*/
/*                     {{ text_domestic_19 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_22 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_22" value="1" checked="checked" />*/
/*                     {{ text_domestic_22 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_22" value="1" />*/
/*                     {{ text_domestic_22 }}*/
/*                    {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_23 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_23" value="1" checked="checked" />*/
/*                     {{ text_domestic_23 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_23" value="1" />*/
/*                     {{ text_domestic_23 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_25 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_25" value="1" checked="checked" />*/
/*                     {{ text_domestic_25 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_25" value="1" />*/
/*                     {{ text_domestic_25 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_27 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_27" value="1" checked="checked" />*/
/*                     {{ text_domestic_27 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_27" value="1" />*/
/*                     {{ text_domestic_27 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_domestic_28 %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_28" value="1" checked="checked" />*/
/*                     {{ text_domestic_28 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_domestic_28" value="1" />*/
/*                     {{ text_domestic_28 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*               </div>*/
/*               <button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-link">{{ text_select_all }}</button> / <button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-link">{{ text_unselect_all }}</button></div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label">{{ entry_international }}</label>*/
/*             <div class="col-sm-10">*/
/*               <div class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_1 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_1" value="1" checked="checked" />*/
/*                     {{ text_international_1 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_1" value="1" />*/
/*                     {{ text_international_1 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_2 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_2" value="1" checked="checked" />*/
/*                     {{ text_international_2 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_2" value="1" />*/
/*                     {{ text_international_2 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_4 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_4" value="1" checked="checked" />*/
/*                     {{ text_international_4 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_4" value="1" />*/
/*                     {{ text_international_4 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_5 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_5" value="1" checked="checked" />*/
/*                     {{ text_international_5 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_5" value="1" />*/
/*                     {{ text_international_5 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_6 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_6" value="1" checked="checked" />*/
/*                     {{ text_international_6 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_6" value="1" />*/
/*                     {{ text_international_6 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_7 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_7" value="1" checked="checked" />*/
/*                     {{ text_international_7 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_7" value="1" />*/
/*                     {{ text_international_7 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_8 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_8" value="1" checked="checked" />*/
/*                     {{ text_international_8 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_8" value="1" />*/
/*                     {{ text_international_8 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_9 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_9" value="1" checked="checked" />*/
/*                     {{ text_international_9 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_9" value="1" />*/
/*                     {{ text_international_9 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_10 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_10" value="1" checked="checked" />*/
/*                     {{ text_international_10 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_10" value="1" />*/
/*                     {{ text_international_10 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_11 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_11" value="1" checked="checked" />*/
/*                     {{ text_international_11 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_11" value="1" />*/
/*                     {{ text_international_11 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_12 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_12" value="1" checked="checked" />*/
/*                     {{ text_international_12 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_12" value="1" />*/
/*                     {{ text_international_12 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_13 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_13" value="1" checked="checked" />*/
/*                     {{ text_international_13 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_13" value="1" />*/
/*                     {{ text_international_13 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_14 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_14" value="1" checked="checked" />*/
/*                     {{ text_international_14 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_14" value="1" />*/
/*                     {{ text_international_14 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_15 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_15" value="1" checked="checked" />*/
/*                     {{ text_international_15 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_15" value="1" />*/
/*                     {{ text_international_15 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_16 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_16" value="1" checked="checked" />*/
/*                     {{ text_international_16 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_16" value="1" />*/
/*                     {{ text_international_16 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*                 <div class="checkbox">*/
/*                   <label>*/
/*                     {% if shipping_usps_international_21 %}*/
/*                     <input type="checkbox" name="shipping_usps_international_21" value="1" checked="checked" />*/
/*                     {{ text_international_21 }}*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="shipping_usps_international_21" value="1" />*/
/*                     {{ text_international_21 }}*/
/*                     {% endif %}*/
/*                   </label>*/
/*                 </div>*/
/*               </div>*/
/*               <button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', true);" class="btn btn-link">{{ text_select_all }}</button> / <button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', false);" class="btn btn-link">{{ text_unselect_all }}</button></div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-size">{{ entry_size }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_size" id="input-size" class="form-control">*/
/*                 {% for size in sizes %}*/
/*                 {% if size.value == shipping_usps_size %}*/
/*                 <option value="{{ size.value }}" selected="selected">{{ size.text }}</option>*/
/*                 {% else %}*/
/*                 <option value="{{ size.value }}">{{ size.text }}</option>*/
/*                {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-container">{{ entry_container }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_container" id="input-container" class="form-control">*/
/*                 {% for container in containers %}*/
/*                 {% if container.value == shipping_usps_container %}*/
/*                 <option value="{{ container.value }}" selected="selected">{{ container.text }}</option>*/
/*                 {% else %}*/
/*                 <option value="{{ container.value }}">{{ container.text }}</option>*/
/*                 {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-machinable">{{ entry_machinable }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_machinable" id="input-machinable" class="form-control">*/
/*                 {% if shipping_usps_machinable %}*/
/*                 <option value="1" selected="selected">{{ text_yes }}</option>*/
/*                 <option value="0">{{ text_no }}</option>*/
/*                 {% else %}*/
/*                 <option value="1">{{ text_yes }}</option>*/
/*                 <option value="0" selected="selected">{{ text_no }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-length"><span data-toggle="tooltip" title="{{ help_dimension }}">{{ entry_dimension }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <div class="row">*/
/*                 <div class="col-sm-4">*/
/*                   <input type="text" name="shipping_usps_length" value="{{ shipping_usps_length }}" placeholder="{{ entry_length }}" id="input-length" class="form-control" />*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                   <input type="text" name="shipping_usps_width" value="{{ shipping_usps_width }}" placeholder="{{ entry_width }}" id="input-width" class="form-control" />*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                   <input type="text" name="shipping_usps_height" value="{{ shipping_usps_height }}" placeholder="{{ entry_height }}" id="input-height" class="form-control" />*/
/*                 </div>*/
/*               </div>*/
/*               {% if error_dimension %}*/
/*               <div class="text-danger">{{ error_dimension }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_display_time }}">{{ entry_display_time }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <label class="radio-inline">*/
/*                 {% if shipping_usps_display_time %}*/
/*                 <input type="radio" name="shipping_usps_display_time" value="1" checked="checked" />*/
/*                 {{ text_yes }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="shipping_usps_display_time" value="1" />*/
/*                 {{ text_yes }}*/
/*                 {% endif %}*/
/*               </label>*/
/*               <label class="radio-inline">*/
/*                 {% if not shipping_usps_display_time %}*/
/*                 <input type="radio" name="shipping_usps_display_time" value="0" checked="checked" />*/
/*                 {{ text_no }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="shipping_usps_display_time" value="0" />*/
/*                 {{ text_no }}*/
/*                 {% endif %}*/
/*               </label>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_display_weight }}">{{ entry_display_weight }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <label class="radio-inline">*/
/*                 {% if shipping_usps_display_weight %}*/
/*                 <input type="radio" name="shipping_usps_display_weight" value="1" checked="checked" />*/
/*                 {{ text_yes }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="shipping_usps_display_weight" value="1" />*/
/*                 {{ text_yes }}*/
/*                 {% endif %}*/
/*               </label>*/
/*               <label class="radio-inline">*/
/*                 {% if not shipping_usps_display_weight %}*/
/*                 <input type="radio" name="shipping_usps_display_weight" value="0" checked="checked" />*/
/*                 {{ text_no }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="shipping_usps_display_weight" value="0" />*/
/*                 {{ text_no }}*/
/*                 {% endif %}*/
/*               </label>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-weight-class"><span data-toggle="tooltip" title="{{ help_weight_class }}">{{ entry_weight_class }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_weight_class_id" id="input-weight-class" class="form-control">*/
/*                 {% for weight_class in weight_classes %}*/
/*                 {% if weight_class.weight_class_id == shipping_usps_weight_class_id %}*/
/*                 <option value="{{ weight_class.weight_class_id }}" selected="selected">{{ weight_class.title }}</option>*/
/*                 {% else %}*/
/*                 <option value="{{ weight_class.weight_class_id }}">{{ weight_class.title }}</option>*/
/*                 {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-tax-class">{{ entry_tax }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_tax_class_id" id="input-tax-class" class="form-control">*/
/*                 <option value="0">{{ text_none }}</option>*/
/*                 {% for tax_class in tax_classes %}*/
/*                 {% if tax_class.tax_class_id == shipping_usps_tax_class_id %}*/
/*                 <option value="{{ tax_class.tax_class_id }}" selected="selected">{{ tax_class.title }}</option>*/
/*                 {% else %}*/
/*                 <option value="{{ tax_class.tax_class_id }}">{{ tax_class.title }}</option>*/
/*                 {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-geo-zone">{{ entry_geo_zone }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_geo_zone_id" id="input-geo-zone" class="form-control">*/
/*                 <option value="0">{{ text_all_zones }}</option>*/
/*                 {% for geo_zone in geo_zones %}*/
/*                 {% if geo_zone.geo_zone_id == shipping_usps_geo_zone_id %}*/
/*                 <option value="{{ geo_zone.geo_zone_id }}" selected="selected">{{ geo_zone.name }}</option>*/
/*                 {% else %}*/
/*                 <option value="{{ geo_zone.geo_zone_id }}">{{ geo_zone.name }}</option>*/
/*                 {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_status" id="input-status" class="form-control">*/
/*                 {% if shipping_usps_status %}*/
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 {% else %}*/
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-sort-order">{{ entry_sort_order }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="shipping_usps_sort_order" value="{{ shipping_usps_sort_order }}" placeholder="{{ entry_sort_order }}" id="input-sort-order" class="form-control" />*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-debug"><span data-toggle="tooltip" title="{{ help_debug }}">{{ entry_debug }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <select name="shipping_usps_debug" id="input-debug" class="form-control">*/
/*                 {% if shipping_usps_debug %}*/
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 {% else %}*/
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
/* */
