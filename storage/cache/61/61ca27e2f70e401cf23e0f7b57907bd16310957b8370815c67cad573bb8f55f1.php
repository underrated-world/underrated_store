<?php

/* default/template/mail/affiliate_alert.twig */
class __TwigTemplate_e2dabfb4053f94ec2f7654e057c16d50f559735509266906b7e8bab83ce2d8e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["text_signup"]) ? $context["text_signup"] : null);
        echo "

";
        // line 3
        if ((isset($context["company"]) ? $context["company"] : null)) {
            // line 4
            echo (isset($context["text_company"]) ? $context["text_company"] : null);
            echo " ";
            echo (isset($context["customer_group"]) ? $context["customer_group"] : null);
            echo "
";
        }
        // line 6
        echo (isset($context["text_website"]) ? $context["text_website"] : null);
        echo " ";
        echo (isset($context["website"]) ? $context["website"] : null);
        echo "
";
        // line 7
        echo (isset($context["text_firstname"]) ? $context["text_firstname"] : null);
        echo " ";
        echo (isset($context["firstname"]) ? $context["firstname"] : null);
        echo "
";
        // line 8
        echo (isset($context["text_lastname"]) ? $context["text_lastname"] : null);
        echo " ";
        echo (isset($context["lastname"]) ? $context["lastname"] : null);
        echo "
";
        // line 9
        if ((isset($context["customer_group"]) ? $context["customer_group"] : null)) {
            // line 10
            echo (isset($context["text_customer_group"]) ? $context["text_customer_group"] : null);
            echo " ";
            echo (isset($context["customer_group"]) ? $context["customer_group"] : null);
            echo "
";
        }
        // line 12
        echo (isset($context["text_email"]) ? $context["text_email"] : null);
        echo " ";
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "
";
        // line 13
        echo (isset($context["text_telephone"]) ? $context["text_telephone"] : null);
        echo " ";
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/template/mail/affiliate_alert.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 13,  60 => 12,  53 => 10,  51 => 9,  45 => 8,  39 => 7,  33 => 6,  26 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ text_signup }}*/
/* */
/* {% if company %}*/
/* {{ text_company }} {{ customer_group }}*/
/* {% endif %}*/
/* {{ text_website }} {{ website }}*/
/* {{ text_firstname }} {{ firstname }}*/
/* {{ text_lastname }} {{ lastname }}*/
/* {% if customer_group %}*/
/* {{ text_customer_group }} {{ customer_group }}*/
/* {% endif %}*/
/* {{ text_email }} {{ email }}*/
/* {{ text_telephone }} {{ telephone }}*/
/* */
